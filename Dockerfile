FROM linuxserver/letsencrypt:1.6.0-ls130
LABEL maintainer="CyanCor GmbH - https://cyancor.com/"

ENV PUID=1000 \
    PGID=1000 \
    TZ=Europe/Berlin \
    VALIDATION=http \
    ONLY_SUBDOMAINS=true